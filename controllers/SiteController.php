<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ArrayDataProvider;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $results = [
            [
                'name' => 'Иванов',
                'date' => '15.04.2016',
                'score' => 5
            ],
            [
                'name' => 'Петров',
                'date' => '16.04.2016',
                'score' => 4
            ],
            [
                'name' => 'Сидоров',
                'date' => '17.04.2016',
                'score' => 3
            ],
            [
                'name' => 'Сидоров',
                'date' => '17.04.2016',
                'score' => 4
            ],
            [
                'name' => 'Петров',
                'date' => '17.04.2016',
                'score' => 8
            ],

        ];

        $data = [];
        foreach ($results as $result) {
            $p = [];
            $data[$result['name']][] =
                [
                'name' => $result['name'],
                'date' => $result['date'],
                'score' => $result['score'],
            ];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $results,

        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'results' => $results
        ]);
    }
}
