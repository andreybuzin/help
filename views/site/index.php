<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <?php

        if (isset($dataProvider)) {
            $columns = [['class' => 'yii\grid\SerialColumn'], 'name'];
          foreach(ArrayHelper::index($results, 'date') as $date => $a) {
                //$currentDate = $result['date'];
                $columns[] = [
                    'header' => $date,
                    'value' => function($model) use ($date, $results) {
                        $scores = [];
                        foreach ($results as $result) {
                            if ($model['name'] === $result['name'] && $model['date'] === $result['date']) {
                                $scores[] = $result['score'];
                            }
                        }
                        return implode(', ', $scores);
                    }
                ];
            }

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'emptyCell' => '-',
                'columns' => $columns
            ]);
        }

        ?>
    </div>
</div>
